This is a website I created for the Development Economics research group at UC San Diego. 
It was made using HTML, CSS and JavaScript. 
This was designed as a prototype/template to build off of for the final version of the website.

It was made with the help of the Kreo template: 
https://www.styleshout.com/free-templates/kreo/

Not all files were wrote completely by me and credit is given as appropriate.
The majority of files that are heavily edited are HTML, with only minor tweaks
to the prexisitng JavaScript.

